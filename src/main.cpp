#include <iostream>
#include <random>
#include <vector>
#include <string>
#include <chrono>
#include <fstream>
#include <filesystem>

namespace fs = std::filesystem;

struct IsingSimulation {
    using SpinModel = std::vector<int32_t>;

private:

    double j{};
    double h{};
    double k{};
    double t{};
    int spinCount;

    SpinModel spinModel;
    std::vector<double> energyArray;
    std::vector<double> magnetizationArray;
    std::vector<double> capacityArray;

    std::default_random_engine engine;
    std::uniform_int_distribution<int> getRandIndex;
    std::uniform_real_distribution<float> getRandPercentage;

public:

    IsingSimulation(int spinCount, double j, double h, double k, double t)
            : spinCount(spinCount), j(j), h(h), k(k), t(t), spinModel(SpinModel(spinCount, 1)),
              engine(std::chrono::system_clock::now().time_since_epoch().count()),
              getRandIndex(std::uniform_int_distribution<int>(0, spinCount - 1)),
              getRandPercentage(std::uniform_real_distribution<float>(0, 1)) {
    }

    void MetroEvolve() {
        for (size_t index = 0; index < 10 * spinCount; ++index) {
            energyArray.push_back(GetEnergy(spinModel));
            const auto spinIndex = getRandIndex(engine);
            auto newSpinModel = spinModel;
            newSpinModel[spinIndex] *= -1;

            const auto oldEnergy = GetEnergy(spinModel);
            const auto newEnergy = GetEnergy(newSpinModel);
            const auto deltaEnergy = newEnergy - oldEnergy;
            if (deltaEnergy <= 0) {
                spinModel = newSpinModel;
                continue;
            }

            const auto chance = std::exp((-deltaEnergy / (k * t)));
            const auto randValue = getRandPercentage(engine);
            if (chance >= randValue) {
                spinModel = newSpinModel;
                continue;
            }
        }
    }

    void SaveResult(std::ofstream stream) {
        stream << "\nMetro\n";
        stream << "Spin count: " + std::to_string(spinModel.size()) + "\n";
        stream << "Configuration: ";
        for (auto element : spinModel)
            stream << (element == 1 ? "1" : "0");
        stream << "\nWhen positive = 1, negative = 0;\n";

        stream << "\nEnergy:\n";
        for (auto &element : energyArray)
            stream << std::to_string(element) + "\n";
    }

private:

    [[nodiscard]] double GetEnergy(const SpinModel &localSpinModel) const {
        int32_t interactionVal = 0;
        int32_t magneticVal = 0;
        const size_t Size = localSpinModel.size();
        for (size_t index = 0; index < Size; ++index) {
            if ((index + 1) != Size)
                interactionVal += localSpinModel[index] * localSpinModel[index + 1];
            else {
                interactionVal += localSpinModel[index] * localSpinModel[0];
            }
            magneticVal += localSpinModel[index];
        }
        return static_cast<double>(-0.25 * j * interactionVal - 0.5 * h * magneticVal);
    }
};

struct IsingSimulationWithTemperature {
    using SpinModel = std::vector<int32_t>;

private:
    double j{};
    double h{};
    double k{};
    double tBegin{};
    double tEnd{};
    double tDelta{};
    int spinCount;

    SpinModel spinModel;
    std::vector<double> temperature;
    std::vector<double> energy;
    std::vector<double> magnetization;
    std::vector<double> capacity;

    std::default_random_engine engine;
    std::uniform_int_distribution<int> getRandIndex;
    std::uniform_real_distribution<float> getRandPercentage;

public:

    IsingSimulationWithTemperature(int spinCount, double j, double h, double k, double tBegin, double tEnd,
                                   double tDelta)
            : spinCount(spinCount), j(j), h(h), k(k), tBegin(tBegin), tEnd(tEnd), tDelta(tDelta),
              spinModel(SpinModel(spinCount, 1)),
              engine(std::chrono::system_clock::now().time_since_epoch().count()),
              getRandIndex(std::uniform_int_distribution<int>(0, spinCount - 1)),
              getRandPercentage(std::uniform_real_distribution<float>(0, 1)) {
    }

    void MetroEvolve() {
        for (double tCurrent = tBegin; tCurrent < tEnd; tCurrent += tDelta) {
            LocalMetroEvolve(tCurrent);
        }
    }

    void SaveResult(std::ofstream stream) {
        stream << "\nMetro\n";
        stream << "Spin count: " + std::to_string(spinModel.size()) + "\n";

        stream << "\nTemperature:\n";
        for (auto &element : temperature)
            stream << std::to_string(element) + "\n";

        stream << "\nEnergy:\n";
        for (auto &element : energy)
            stream << std::to_string(element) + "\n";

        stream << "\nMagnetization:\n";
        for (auto &element : magnetization)
            stream << std::to_string(element) + "\n";

        stream << "\nCapacity:\n";
        for (auto &element : capacity)
            stream << std::to_string(element) + "\n";
    }

private:

    void LocalMetroEvolve(const double t) {
        for (size_t index = 0; index < 10 * spinCount; ++index) {
            const auto spinIndex = getRandIndex(engine);
            auto newSpinModel = spinModel;
            newSpinModel[spinIndex] *= -1;

            const auto oldEnergy = GetEnergy(spinModel);
            const auto newEnergy = GetEnergy(newSpinModel);
            const auto deltaEnergy = newEnergy - oldEnergy;
            if (deltaEnergy <= 0) {
                spinModel = newSpinModel;
                continue;
            }

            const auto chance = std::exp((-deltaEnergy / (k * t)));
            const auto randValue = getRandPercentage(engine);
            if (chance >= randValue) {
                spinModel = newSpinModel;
                continue;
            }
        }

        temperature.push_back(t);
        energy.push_back(GetEnergy(spinModel));
        magnetization.push_back(GetMagnetization(spinModel));
        capacity.push_back(GetCapacity(spinModel, t));
    }

    [[nodiscard]] double GetEnergy(const SpinModel &localSpinModel) const {
        int32_t interactionVal = 0;
        int32_t magneticVal = 0;
        const size_t Size = localSpinModel.size();
        for (size_t index = 0; index < Size; ++index) {
            if ((index + 1) != Size)
                interactionVal += localSpinModel[index] * localSpinModel[index + 1];
            else {
                interactionVal += localSpinModel[index] * localSpinModel[0];
            }
            magneticVal += localSpinModel[index];
        }
        return static_cast<double>(-0.25 * j * interactionVal - 0.5 * h * magneticVal);
    }

    [[nodiscard]] double GetMagnetization(const SpinModel &localSpinModel) const {
        int32_t magneticVal = 0;
        const size_t Size = localSpinModel.size();
        for (size_t index = 0; index < Size; ++index) {
            magneticVal += localSpinModel[index];
        }
        return static_cast<double>(0.5 * h * magneticVal) / static_cast<double>(spinCount);
    }

    [[nodiscard]] double GetCapacity(const SpinModel &localSpinModel, double t) const {
        double sumLocalEnergy = 0; //<E^2>
        {
            const size_t Size = localSpinModel.size();
            for (size_t index = 0; index < Size; ++index) {
                if ((index + 1) != Size) {
                    sumLocalEnergy += (-0.25 * j * localSpinModel[index] * localSpinModel[index + 1] -
                                    0.5 * h * localSpinModel[index])
                                   * (-0.25 * j * localSpinModel[index] * localSpinModel[index + 1] -
                                      0.5 * h * localSpinModel[index]);
                } else {
                    sumLocalEnergy += (-0.25 * j * localSpinModel[index] * localSpinModel[0]
                                    - 0.5 * h * localSpinModel[index])
                                   * (-0.25 * j * localSpinModel[index] * localSpinModel[0] -
                                      0.5 * h * localSpinModel[index]);
                }
            }
        }
        double localEnergy = GetEnergy(localSpinModel);
        return (sumLocalEnergy - localEnergy * localEnergy) / (spinCount * k * t * t);
    }
};


int main() {
    constexpr int spinCount = 2000;
    constexpr float j = 1;
    constexpr float h = 0.01;
    constexpr float k = 1;
    //constexpr float t = 300;

    IsingSimulationWithTemperature isingSimulation(spinCount, j, h, k, 1, 20, 1);
    isingSimulation.MetroEvolve();

    std::ofstream out;
    out.open(fs::current_path().string() + std::string("/Output.txt"));
    if (out.is_open()) {
        isingSimulation.SaveResult(std::move(out));
    }

    return 0;
}
